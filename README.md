# KeyCRM OpenCart Module

Этот модуль позволяет интегрировать CMS Opencart >= 2.3 c [KeyCRM](https://keycrm.app).

### Порядок внесения изменений

При создании PR с правками, также нужно изменить версию в файле VERSION в корне проекта.
Эта версия автоматом подставится в шаблон, который будет виден в админке Opencart, а также создастся архив с нужными файлами (в артефактах)

Также последнюю версию модуля всегда можно скачать по ссылке:

https://gitlab.com/GetBetter/key-crm/shopping-cart-plugins/opencart/-/jobs/artifacts/master/download?job=build-opencart
