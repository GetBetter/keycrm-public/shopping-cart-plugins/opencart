<?php

namespace KeyCRM\Api\Model;

class Buyer
{
    /** @var string */
    public $full_name;
    /** @var string */
    public $email;
    /** @var string */
    public $phone;
}