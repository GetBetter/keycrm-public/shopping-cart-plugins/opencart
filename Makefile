ROOT_DIR=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
VERSION = `cat $(ROOT_DIR)/VERSION`
ARCHIVE_NAME = '../build/keycrm-'$(VERSION)'.ocmod.zip'

# Запускает сервер с opencart и следит за изменениями в каталоге src
watch:
	docker-compose up -d
	docker-compose exec oc_php-fpm vendor/bin/robo --load-from tests/RoboScript.php opencart:watch

# Запускает сервер с opencart
run:
	docker-compose up -d
	docker-compose exec oc_php-fpm vendor/bin/robo --load-from tests/RoboScript.php opencart:run

# Создает архив с модулем
archive:
	mkdir -p ./build
	(cd ./src && zip -r $(ARCHIVE_NAME) ./*)
