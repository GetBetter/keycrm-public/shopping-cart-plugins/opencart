<?php

use Robo\Tasks;

require_once(__DIR__ . '/../vendor/autoload.php');

class RoboScript extends Tasks
{
    private const ROOT_DIR = __DIR__ . '/..';
    private const WEB_DIR = __DIR__ . '/../web';

    private const MYSQL_HOST = 'oc_mysql';
    private const MYSQL_PORT = '3306';

    public function __construct()
    {
        $dotenv = Dotenv\Dotenv::createImmutable(self::ROOT_DIR);
        $dotenv->safeLoad();
    }

    /**
     * Настраивает opencart и запускает сервер
     */
    public function opencartRun(): void
    {
        $this->opencartSetup();

        $this
            ->taskServer($_ENV['SERVER_PORT'])
            ->dir(self::WEB_DIR . '/upload')
            ->host('0.0.0.0')
            ->run();
    }

    public function opencartWatch(): void
    {
        $this->opencartSetup();

        $this
            ->taskServer($_ENV['SERVER_PORT'])
            ->dir(self::WEB_DIR . '/upload')
            ->host('0.0.0.0')
            ->background()
            ->run();

        $this->taskWatch()
            ->monitor(self::ROOT_DIR . '/src/', function () {
                $this->installModule();
            })
            ->run();
    }

    public function installModule(): void
    {
        $this
            ->taskCopyDir([
                self::ROOT_DIR . '/src/upload/' => self::WEB_DIR . '/upload/'
            ])
            ->run();
    }

    /**
     * Настраивает opencart и загружает демо данные
     */
    protected function opencartSetup(): void
    {
        $this->taskDeleteDir(self::WEB_DIR)->run();
        $this->taskFileSystemStack()
            ->mirror(
                self::ROOT_DIR . '/vendor/opencart/opencart',
                self::WEB_DIR
            )
            ->chmod(self::WEB_DIR, 0777, 0000, true)
            ->run();

        $this->taskDeleteDir(self::WEB_DIR . '/upload/system/storage/vendor')->run();
        $this
            ->taskComposerInstall()
            ->workingDir(self::WEB_DIR)
            ->run();

        // Fix opencart issues with mysql 8
        $this->taskReplaceInFile(self::WEB_DIR . '/upload/install/cli_install.php')
            ->from('$db->query("SET @@session.sql_mode = \'MYSQL40\'");')
            ->to('//$db->query("SET @@session.sql_mode = \'MYSQL40\'");')
            ->run();

        $this->taskReplaceInFile(self::WEB_DIR . '/upload/system/library/db/mysqli.php')
            ->from('$this->connection->query("SET SESSION sql_mode = \'NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION\'");')
            ->to('$this->connection->query("SET SESSION sql_mode = \'NO_ZERO_IN_DATE,NO_ENGINE_SUBSTITUTION\'");')
            ->run();

        // Install opencart
        $this->waitForDB();
        $this->taskExec('php')
            ->arg(self::WEB_DIR . '/upload/install/cli_install.php')
            ->arg('install')
            ->options([
                'db_hostname' => self::MYSQL_HOST,
                'db_database' => $_ENV['MYSQL_DATABASE'],
                'db_username' => $_ENV['MYSQL_USER'],
                'db_password' => $_ENV['MYSQL_PASSWORD'],

                'db_port'     => self::MYSQL_PORT,
                'username'    => $_ENV['OC_USERNAME'],
                'password'    => $_ENV['OC_PASSWORD'],
                'email'       => $_ENV['OC_EMAIL'],
                'http_server' => sprintf('http://localhost:%s/', $_ENV['SERVER_PORT']),
            ])
            ->run();

        $this
            ->taskCopyDir([
                self::WEB_DIR . '/upload/system/storage' => self::WEB_DIR . '/storage'
            ])
            ->run();

        $this
            ->taskReplaceInFile(self::WEB_DIR . '/upload/config.php')
            ->from("define('DIR_STORAGE', DIR_SYSTEM . 'storage/');")
            ->to("define('DIR_STORAGE', '/opencart-module/web/storage/');")
            ->run();

        $this
            ->taskReplaceInFile(self::WEB_DIR . '/upload/admin/config.php')
            ->from("define('DIR_STORAGE', DIR_SYSTEM . 'storage/');")
            ->to("define('DIR_STORAGE', '/opencart-module/web/storage/');")
            ->run();

        $this->taskDeleteDir(self::WEB_DIR . '/upload/system/storage')->run();
        $this->taskDeleteDir(self::WEB_DIR . '/upload/install')->run();

        $this->installModule();

        echo "\n";
        echo "\n" . "Opencart started";
        echo "\n" . sprintf("Store link: http://localhost:%s/", $_ENV['SERVER_PORT']);
        echo "\n" . sprintf("Admin link: http://localhost:%s/admin", $_ENV['SERVER_PORT']);
        echo "\n\n" . sprintf("Admin credentials: %s / %s", $_ENV['OC_USERNAME'], $_ENV['OC_PASSWORD']);
        echo "\n\n\n";
    }

    protected function waitForDB(): void
    {
        $oldErrorReporting = error_reporting();
        $success = false;
        for ($i = 0; $i < 10; $i++) {
            echo "\nWait for database...\n";

            error_reporting(0);
            $conn = new mysqli(
                self::MYSQL_HOST,
                $_ENV['MYSQL_DATABASE'],
                $_ENV['MYSQL_USER'],
                $_ENV['MYSQL_PASSWORD'],
                self::MYSQL_PORT
            );

            if ($conn->connect_error) {
                sleep(5);
                continue;
            }

            $success = true;
            break;
        }

        error_reporting($oldErrorReporting);
        if (! $success) {
            die("Unable to connect to database\n");
        }
    }
}
