<?php

$_['heading_title'] = 'KeyCRM';
$_['text_module'] = 'Modules';
$_['general_tab_text'] = 'General';

$_['keycrm_url'] = 'KeyCRM URL';
$_['keycrm_api_key'] = 'KeyCRM API key';
$_['keycrm_stores'] = 'Stores';

$_['stores_table_store'] = 'Store';
$_['stores_table_source'] = 'KeyCRM source';
$_['stores_table_enabled'] = 'Enabled';
$_['api_connection_refused'] = 'Unable to connect to KeyCRM API';
$_['logs_tab_text'] = 'Logs';

$_['export_tab_text'] = 'Orders export';

$_['text_button_export'] = 'Export orders';
$_['text_button_export_one'] = 'Export';
$_['text_export_help'] = 'New orders will come into KeyCRM in case you made a setup on General tab';
$_['text_export_help2'] = 'Would you like export all orders history from OpenCart into KeyCRM - please, use button below. Wait for success status after clicking (1-2 minutes per 1000 orders) and check Logs tab for errors';
$_['text_export_one'] = 'In case you want to upload one order from OpenCart to KeyCRM, use the button below.';
$_['text_placeholder_export_one'] = 'Enter Opencart order number';
$_['text_success_export'] = 'Orders have been successfully uploaded';
$_['text_button_clear'] = 'Clear';

$_['entry_status'] = 'Module Status';

$_['set_apikey_and_save'] = 'Specify API key and save settings';
$_['select_source_and_save'] = 'Select source and save settings';
$_['keycrm_payment_methods'] = 'Payment methods';
$_['keycrm_shipping_methods'] = 'Shipping methods';
$_['create_payments_text'] = 'Create payments';
$_['create_shippings_text'] = 'Create a delivery service';
$_['order_statuses_tab_text'] = 'Order Statuses';
$_['text_heading_order_statuses_settings'] = 'Select the order statuses to be uploaded';
$_['text_abandoned_cart'] = 'Abandoned cart';

$_['payments_tab_text'] = 'Payment methods';
$_['shipping_tab_text'] = 'Shipping methods';
$_['payment_methods_table_oc_method'] = 'Opencart payment method';
$_['payment_methods_table_kc_method'] = 'KeyCRM payment method';
$_['shipping_methods_table_oc_method'] = 'Opencart shipping method';
$_['shipping_methods_table_kc_method'] = 'KeyCRM shipping method';
$_['shipping_methods_not_found'] = 'No active shipping methods';

// Button
$_['button_status_settings'] = 'Select order statuses for unloading';