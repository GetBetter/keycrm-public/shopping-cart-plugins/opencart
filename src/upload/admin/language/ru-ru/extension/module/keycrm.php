<?php

$_['heading_title'] = 'KeyCRM';
$_['text_module'] = 'Модули';
$_['general_tab_text'] = 'Основные';

$_['keycrm_url'] = 'Адрес KeyCRM';
$_['keycrm_api_key'] = 'Ключ API KeyCRM';
$_['keycrm_stores'] = 'Магазины';

$_['stores_table_store'] = 'Магазин';
$_['stores_table_source'] = 'Источник KeyCRM';
$_['stores_table_enabled'] = 'Включено';
$_['api_connection_refused'] = 'Не удалось подключиться к API KeyCRM';
$_['logs_tab_text'] = 'Журнал ошибок';

$_['export_tab_text'] = 'Экспорт заказов';

$_['text_button_export'] = 'Экспортировать заказы';
$_['text_button_export_one'] = 'Экспортировать';
$_['text_export_help'] = 'Если вы настроили модуль KeyCRM на вкладке «Основные», то новые заказы УЖЕ будут поступать в кабинет KeyCRM';
$_['text_export_help2'] = 'Если же вы хотите выгрузить и всю историю заказов из OpenCart в KeyCRM - воспользуйтесь кнопкой ниже. После нажатия - дождитесь успешного статуса (каждые 1000 заказов займут где-то 1-2 минуты), после чего можете проверить вкладку «Журнал ошибок» на наличие проблем при экспорте';
$_['text_export_one'] = 'Если же вы хотите выгрузить один заказ из OpenCart в KeyCRM - воспользуйтесь кнопкой ниже.';
$_['text_placeholder_export_one'] = 'Введите номер заказа в Opencart';
$_['text_success_export'] = 'Заказы успешно выгруженны';
$_['text_button_clear'] = 'Очистить';

$_['entry_status'] = 'Статус модуля';

$_['set_apikey_and_save'] = 'Укажите ключ и сохраните настройки';
$_['select_source_and_save'] = 'Выберите источник и сохраните настройки';
$_['keycrm_payment_methods'] = 'Типы оплат';
$_['keycrm_shipping_methods'] = 'Типы доставки';
$_['create_payments_text'] = 'Создавать оплаты';
$_['create_shippings_text'] = 'Создавать службу доставки';
$_['order_statuses_tab_text'] = 'Статусы заказов';
$_['text_heading_order_statuses_settings'] = 'Выберите статусы заказов которые нужно загружать';
$_['text_abandoned_cart'] = 'Брошенная корзина';

$_['payments_tab_text'] = 'Типы оплаты';
$_['shipping_tab_text'] = 'Типы доставки';
$_['payment_methods_table_oc_method'] = 'Тип оплаты Opencart';
$_['payment_methods_table_kc_method'] = 'Тип оплаты KeyCRM';
$_['shipping_methods_table_oc_method'] = 'Тип доставки Opencart';
$_['shipping_methods_table_kc_method'] = 'Тип доставки KeyCRM';
$_['shipping_methods_not_found'] = 'Нет активных доставок';

// Button
$_['button_status_settings'] = 'Выбрать статусы заказов для выгрузки';