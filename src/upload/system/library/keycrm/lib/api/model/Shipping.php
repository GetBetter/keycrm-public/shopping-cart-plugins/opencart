<?php

namespace KeyCRM\Api\Model;

class Shipping
{
    /** @var string */
    public $delivery_service_id;
    /** @var string */
    public $shipping_address_country;
    /** @var string */
    public $shipping_address_city;
    /** @var string */
    public $shipping_address_region;
    /** @var string */
    public $shipping_address_zip;
    /** @var string */
    public $shipping_receive_point;
    /** @var string */
    public $recipient_full_name;
    /** @var string */
    public $recipient_phone;
}