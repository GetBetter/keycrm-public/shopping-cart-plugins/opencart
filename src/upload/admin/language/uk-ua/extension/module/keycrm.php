<?php

$_['heading_title'] = 'KeyCRM';
$_['text_module'] = 'Модулі';
$_['general_tab_text'] = 'Основні';

$_['keycrm_url'] = 'Адреса KeyCRM';
$_['keycrm_api_key'] = 'Ключ API KeyCRM';
$_['keycrm_stores'] = 'Магазини';

$_['stores_table_store'] = 'Магазин';
$_['stores_table_source'] = 'Джерело KeyCRM';
$_['stores_table_enabled'] = 'Увімкнено';
$_['api_connection_refused'] = 'Не вдалося підключитися до API KeyCRM';
$_['logs_tab_text'] = 'Журнал помилок';

$_['export_tab_text'] = 'Експорт замовлень';

$_['text_button_export'] = 'Експортувати замовлення';
$_['text_button_export_one'] = 'Експортувати';
$_['text_export_help'] = 'Якщо ви налаштували модуль KeyCRM на вкладці «Основні», то нові замовлення ВЖЕ будуть надходити до кабінету KeyCRM';
$_['text_export_help2'] = 'Якщо ж ви хочете вивантажити всю історію замовлень з OpenCart в KeyCRM - скористайтеся кнопкою нижче. Після натискання - дочекайтеся успішного статусу (кожні 1000 замовлень займуть десь 1-2 хвилини), після чого можете перевірити вкладку «Журнал помилок» на наявність проблем під час експорту';
$_['text_export_one'] = 'Якщо ви хочете вивантажити одне замовлення з OpenCart в KeyCRM - скористайтеся кнопкою нижче.';
$_['text_placeholder_export_one'] = 'Введіть номер замовлення в Opencart';
$_['text_success_export'] = 'Замовлення успішно вивантажені';
$_['text_button_clear'] = 'Очистити';

$_['entry_status'] = 'Статус модуля';

$_['set_apikey_and_save'] = 'Вкажіть ключ та збережіть налаштування';
$_['select_source_and_save'] = 'Виберіть джерело та збережіть налаштування';
$_['keycrm_payment_methods'] = 'Типи оплат';
$_['keycrm_shipping_methods'] = 'Типи доставки';
$_['create_payments_text'] = 'Створювати оплату';
$_['create_shippings_text'] = 'Створювати службу доставки';
$_['order_statuses_tab_text'] = 'Статуси замовлень';
$_['text_heading_order_statuses_settings'] = 'Виберіть статуси замовлень, які потрібно завантажувати';
$_['text_abandoned_cart'] = 'Покинутий кошик';

$_['payments_tab_text'] = 'Типи оплати';
$_['shipping_tab_text'] = 'Типи доставки';
$_['payment_methods_table_oc_method'] = 'Тип оплати Opencart';
$_['payment_methods_table_kc_method'] = 'Тип оплати KeyCRM';
$_['shipping_methods_table_oc_method'] = 'Тип доставки Opencart';
$_['shipping_methods_table_kc_method'] = 'Тип доставки KeyCRM';
$_['shipping_methods_not_found'] = 'Немає активних доставок';

// Button
$_['button_status_settings'] = 'Вибрати статуси замовлень для вивантаження';